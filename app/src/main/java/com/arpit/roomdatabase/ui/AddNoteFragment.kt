package com.arpit.roomdatabase.ui

import android.app.AlertDialog
import android.os.Bundle
import android.view.*
import androidx.navigation.Navigation
import com.arpit.roomdatabase.R
import com.arpit.roomdatabase.db.AppDatabase
import com.arpit.roomdatabase.db.Note
import kotlinx.android.synthetic.main.fragment_add_note.*
import kotlinx.coroutines.launch

class AddNoteFragment : BaseFragment() {

    private var _note: Note? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_note, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        arguments?.let {
            _note = AddNoteFragmentArgs.fromBundle(it).note
            edit_text_title.setText(_note?.title)
            edit_text_note.setText(_note?.note)
        }

        button_save.setOnClickListener { view ->
            val noteTitle = edit_text_title.text.toString().trim()
            val noteBody = edit_text_note.text.toString().trim()

            if (noteTitle.isEmpty()) {
                edit_text_title.error = "title required"
                edit_text_title.requestFocus()
                return@setOnClickListener
            }

            if (noteBody.isEmpty()) {
                edit_text_note.error = "note required"
                edit_text_note.requestFocus()
                return@setOnClickListener
            }

            launch {
                context?.let {
                    val note = Note(noteTitle, noteBody)

                    if (_note == null) {
                        AppDatabase(it).getNoteDao().addNote(note)
                        it.showToast("Note Saved")
                    } else {
                        _note!!.id = note.id
                        AppDatabase(it).getNoteDao().updateNote(note)
                        it.showToast("Note Updated")
                    }

                    val action = AddNoteFragmentDirections.actionSaveNote()
                    Navigation.findNavController(view).navigate(action)
                }
            }

        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu, menu)
    }

    private fun deleteNode() {
        AlertDialog.Builder(context).apply {
            setTitle("Are you sure")
            setMessage("This can't be undone")
            setPositiveButton("YES") { _, _ ->
                launch {
                    AppDatabase(context).getNoteDao().deleteNote(_note!!)
                    val action = AddNoteFragmentDirections.actionSaveNote()
                    Navigation.findNavController(view!!).navigate(action)
                }
            }
            setNegativeButton("NO") { _, _ ->

            }
        }.create().show()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.delete -> if (_note != null) deleteNode() else context?.showToast("Cannot Delete")


        }
        return super.onOptionsItemSelected(item)
    }
}
